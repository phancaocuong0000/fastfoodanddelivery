# FastFood Clone Application using MERN Stack and Redux - Hooks

## Requirement
***nodejs >= v14.17.0**
***npm >= 6.14.13**
***yarn >= 1.19.1**

## Clone this project

```
create a folder where you want this project place
cd to this location
##Clone Front-end
git clone https://gitlab.com/phancaocuong0000/fastfoodanddelivery.git
cd to project
npm install 
npm start
##Clone Back-end
git clone https://gitlab.com/phancaocuong0000/fastfoodanddelivery.git
cd to project
npm install 
npm start
